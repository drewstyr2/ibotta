const Express = require('express');
const BodyParser = require('body-parser')
var DataStore = require("./datastore");

const app = Express();
const port = 3000;
var dataStore = new DataStore.AnagramDataStore();


app.use(BodyParser.json());

// add words to the data store
app.post("/words.json", (req, res) => {
    console.log(req.body)
    var wordsArray = req.body.words;
    var responseCode = 201;
    try
    {    
        dataStore.add_words(wordsArray);
    }
    catch(err)
    {
        console.log("client attempted to add non-string to data store");
        console.log(err)
        responseCode = 400;
    }
    res.sendStatus(responseCode);
})

// delete all words from the data store
app.delete("/words.json", (req, res) => {
    dataStore.delete_all_words();
    res.sendStatus(204);
})

// get anagrams for a word
app.get("/anagrams/:word.json", (req, res) => {
    var word = req.params.word;
    var limit = req.query.limit;
    var status = 200;
    
    try
    {
        if (limit && limit <= 0)
        {
            status = 400;
            throw "limit must be > 0";
        }
        var anagrams = dataStore.get_anagrams(word);
        var anagramList = [];
        if(anagrams)
        {
            anagrams.delete(word)
            anagramList = limit ? Array.from(anagrams).slice(0, limit) : Array.from(anagrams)
        }
        res.status(status).json({
            "anagrams": anagramList
        })
        if(anagrams)
        {
            anagrams.add(word)
        }
    }
    catch(err)
    {
        console.log("caught error when retrieving anagrams", err)
        res.sendStatus(status);
    }
    dataStore.logState();
})

// delete a word from the data store
app.delete("/words/:word.json", (req, res) => {
    var wordToDelete = req.params.word;
    dataStore.delete_word(wordToDelete);
    res.sendStatus(204);
})

// optional endpoint to get the word with the most anagrams
app.get("/anagrams/most", (req, res) => {
    var mostAnagramsWord = dataStore.get_most_anagrams_word();
    if(mostAnagramsWord.length > 0)
    {
        res.status(200).json({
            "wordWithMostAnagrams": mostAnagramsWord
        })
    }
    else
    {
        res.sendStatus(404);
    }
})


app.listen(port, () => {
    console.log("Listening on port " + port);
})