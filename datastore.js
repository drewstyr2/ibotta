

/**
 * class for keeping track of anagrams and retrieving them
 * NOTE: if this were production code I would not use a JavaScript map, I would probably use something
 * like redis or some other data store
 */
class AnagramDataStore
{

    constructor()
    {
        this.corpus = {};
        this.mostAnagramsWordSet = new Set();
        this.mostAnagramsWord = "";
    }

    /**
     * sorts a word alphabetically
     * @param {String} word word to sort
     */
    sort_word(word)
    {
        return word.split('').sort().join('');
    }

    /**
     * Returns true if word is a String, false otherwise
     * @param {String} word word to validate
     */
    _validateWord(word) 
    {
        if (!(typeof word === "string" ) && !(word instanceof String))
        {
            console.log("invalid word encountered")
            throw "Entry is not a word";
        }
    }

    /**
     * logs the state of the data store
     */
    logState()
    {
        console.log("state of corpus", this.corpus)
    }

    /**
     * Takes a word and adds it to a list of all anagrams of that word
     * determines anagram by sorting the word and using the sorted result as a key in a map
     * @param {String} word word to map to anagrams
     */
    _mapAnagram(word)
    {
        var sortedWord = this.sort_word(word);
        if(!this.corpus[sortedWord])
        {
            this.corpus[sortedWord] = new Set();
        }
        this.corpus[sortedWord].add(word)
        if(this.mostAnagramsWordSet.size < this.corpus[sortedWord].size)
        {
            this.mostAnagramsWordSet = this.corpus[sortedWord];
            this.mostAnagramsWord = word;
        }
    }

    /**
     * adds an array of words to the data store and maps anagrams
     * validates that each word in the array is a String
     * @param {Array} wordsArray array of words to add to the data store
     */
    add_words(wordsArray)
    {
        console.log("adding words to corpus", wordsArray)
        for(var i = 0; i < wordsArray.length; i = i + 1)
        {
            this._validateWord(wordsArray[i]);
            console.log("added " + wordsArray[i] + " to corpus");
            this._mapAnagram(wordsArray[i]);
            
        }
        this.logState();
    }

    /**
     * find all the anagrams in the data store for the given word
     * @param {String} word word from which to find anagrams
     * @returns {Set} the set of anagrams
     */
    get_anagrams(word)
    {  
        console.log("getting anagrams for " + word);
        return this.corpus[this.sort_word(word)];
    }

    /**
     * removes a single word from the data store
     * @param {String} word word to delete
     */
    delete_word(word)
    {
        console.log("deleting " + word + " from corpus")
        var containerSet = this.corpus[this.sort_word(word)]
        if(containerSet)
        {
            containerSet.delete(word);
        }
        this.logState();
    }

    /**
     * clears the data store
     */
    delete_all_words()
    {
        console.log("deleting all words");
        this.corpus = {};
        this.logState();
    }

    /**
     * get the word that has the most anagrams in the data store
     * @returns {String} the word with the most anagrams
     */
    get_most_anagrams_word()
    {
        return this.mostAnagramsWord;
    }
}

module.exports.AnagramDataStore = AnagramDataStore